# @atlaskit/analytics-listeners

## 2.0.2
- [patch] Clean Changelogs - remove duplicates and empty entries [e7756cd](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/e7756cd)
- [none] Updated dependencies [e7756cd](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/e7756cd)

## 2.0.1
- [none] Updated dependencies [9d20f54](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9d20f54)
  - @atlaskit/docs@4.1.0
  - @atlaskit/analytics-gas-types@2.1.2
  - @atlaskit/analytics-next@2.1.7

## 2.0.0
- [major] client parameter changed to a Promise in the listeners given Confluence gets the AnalyticsWebClient instance asynchronously [628e427](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/628e427)

## 1.2.0
- [patch] Throw error on component construction when client prop is missing rather than silently failing until an event is fired [4bbce97](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4bbce97)
- [minor] Add some debug/error logging to listener which can be enabled via the logLevel prop [191a1ff](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/191a1ff)

## 1.1.1
- [patch] Updated dependencies [1e80619](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e80619)
  - @atlaskit/analytics-next@2.1.4
  - @atlaskit/docs@4.0.0
  - @atlaskit/analytics-gas-types@2.1.1

## 1.1.0
- [minor] Add listener for events fired by core atlaskit components [bcc7d8f](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/bcc7d8f)
- [patch] Updated dependencies [9be1db0](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9be1db0)
  - @atlaskit/analytics-gas-types@2.1.0

## 1.0.2
- [patch] Moved event tag to FabricElementsListener [639ae5e](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/639ae5e)

## 1.0.1
- [patch] code improvements [44e55aa](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/44e55aa)
- [patch] added analytics-listeners package  [8e71e9a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/8e71e9a)
