# @atlaskit/analytics-gas-types

## 2.1.3
- [patch] Update changelogs to remove duplicate [cc58e17](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/cc58e17)
- [none] Updated dependencies [cc58e17](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/cc58e17)
  - @atlaskit/docs@4.1.1

## 2.1.2
- [none] Updated dependencies [9d20f54](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9d20f54)
  - @atlaskit/docs@4.1.0

## 2.1.1
- [patch] Updated dependencies [1e80619](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e80619)
  - @atlaskit/docs@4.0.0

## 2.1.0
- [minor] Export individual event types as constants [9be1db0](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9be1db0)

## 2.0.0
- [major] added new package analytics-gas-types [4d238a6](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4d238a6)
