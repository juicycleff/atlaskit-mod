# Badge

Badges are visual indicators for numeric values such as tallies and scores.
They're commonly used before and after the label of the thing they're
quantifying.

They must be used singly after a single item name, and have only numbers.

* Use lozenges for statuses.
* Use labels to call out tags and high-visibility attributes.
* Use a tooltip if you want to indicate units.

## Installation

```sh
yarn add @atlaskit/badge
```

## Usage

Detailed docs and example usage can be found [here](https://atlaskit.atlassian.com/packages/core/badge).
