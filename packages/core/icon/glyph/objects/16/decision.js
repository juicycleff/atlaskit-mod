'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _index = require('../../../es5/index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Objects16DecisionIcon = function Objects16DecisionIcon(props) {
  return _react2.default.createElement(_index2.default, _extends({ dangerouslySetGlyph: '<svg width="24" height="24" viewBox="0 0 24 24" focusable="false" role="presentation"><path d="M12.988 11a.995.995 0 0 0-.293-.715L8.694 6.292a1 1 0 1 0-1.412 1.416L11 11.418V17a1 1 0 0 0 2 0v-6h-.012zM6 4h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2zm9.304 2.29l-1.485 1.469a1.001 1.001 0 0 0 1.407 1.42l1.485-1.468a1 1 0 1 0-1.407-1.421z" fill="currentColor" fill-rule="evenodd"/></svg>' }, props));
};
exports.default = Objects16DecisionIcon;