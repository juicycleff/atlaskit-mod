'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _index = require('../../../es5/index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Objects16FavoriteIcon = function Objects16FavoriteIcon(props) {
  return _react2.default.createElement(_index2.default, _extends({ dangerouslySetGlyph: '<svg width="24" height="24" viewBox="0 0 24 24" focusable="false" role="presentation"><path d="M6 4h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2zm6 11.662l2.493 1.211a.5.5 0 0 0 .715-.507l-.333-2.894 1.912-2.157a.5.5 0 0 0-.274-.821l-2.736-.564-1.336-2.513a.5.5 0 0 0-.883 0L10.223 9.93l-2.736.564a.5.5 0 0 0-.274.82l1.912 2.158-.333 2.894a.5.5 0 0 0 .715.507L12 15.662z" fill="currentColor" fill-rule="evenodd"/></svg>' }, props));
};
exports.default = Objects16FavoriteIcon;