'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _index = require('../../../es5/index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Objects24ServiceDeskApprovalIcon = function Objects24ServiceDeskApprovalIcon(props) {
  return _react2.default.createElement(_index2.default, _extends({ dangerouslySetGlyph: '<svg width="24" height="24" viewBox="0 0 24 24" focusable="false" role="presentation"><path d="M10.083 17H5a1 1 0 0 0 0 2h5.803a6 6 0 1 0 6.176-8.92A6.981 6.981 0 0 0 13 8.07V7h1a1 1 0 0 0 0-2h-4a1 1 0 1 0 0 2h1v1.07A7.002 7.002 0 0 0 5 15v1h5c0 .34.028.675.083 1zM3 0h18a3 3 0 0 1 3 3v18a3 3 0 0 1-3 3H3a3 3 0 0 1-3-3V3a3 3 0 0 1 3-3zm13 22a6 6 0 1 1 0-12 6 6 0 0 1 0 12zm2.293-8.707L15.5 16.084l-1.263-1.267a1 1 0 0 0-1.417 1.411l1.97 1.978a1 1 0 0 0 1.416.001l3.5-3.5a1 1 0 0 0-1.414-1.414z" fill="currentColor" fill-rule="evenodd"/></svg>' }, props));
};
exports.default = Objects24ServiceDeskApprovalIcon;