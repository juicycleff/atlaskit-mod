'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _index = require('../../../es5/index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Objects24UnwatchIcon = function Objects24UnwatchIcon(props) {
  return _react2.default.createElement(_index2.default, _extends({ dangerouslySetGlyph: '<svg width="24" height="24" viewBox="0 0 24 24" focusable="false" role="presentation"><path d="M3 0h18a3 3 0 0 1 3 3v18a3 3 0 0 1-3 3H3a3 3 0 0 1-3-3V3a3 3 0 0 1 3-3zm0 12c0 2.7 4.12 7 9 7 4.881 0 9-4.3 9-7 0-2.765-4.032-7-9-7s-9 4.235-9 7zm2 0c0-1.968 3.517-5 7-5s7 3.032 7 5c0 1.9-3.6 5-7 5s-7-3.1-7-5zm7 3a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0-2a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" fill="currentColor" fill-rule="evenodd"/></svg>' }, props));
};
exports.default = Objects24UnwatchIcon;