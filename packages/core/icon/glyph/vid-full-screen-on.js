'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _index = require('../es5/index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var VidFullScreenOnIcon = function VidFullScreenOnIcon(props) {
  return _react2.default.createElement(_index2.default, _extends({ dangerouslySetGlyph: '<svg width="24" height="24" viewBox="0 0 24 24" focusable="false" role="presentation"><g fill="currentColor" fill-rule="evenodd"><path d="M19 5.003v2.995a1 1 0 1 0 2 0V4.102C21 3.494 20.507 3 19.9 3H16a1 1 0 0 0 0 2.003h3z" fill-rule="nonzero"/><path d="M13.74 10.294a.997.997 0 0 0 1.407.005l5.152-5.152a1 1 0 0 0-.005-1.407l-.034-.034a.997.997 0 0 0-1.407-.005l-5.152 5.152a1 1 0 0 0 .005 1.407l.034.034z" fill-rule="nonzero"/><path d="M9.067 13.321L3.32 19.066a1.115 1.115 0 0 0 .005 1.57l.036.037a1.112 1.112 0 0 0 1.571.005l5.747-5.744a1.116 1.116 0 0 0-.006-1.57l-.037-.037a1.117 1.117 0 0 0-1.57-.006z"/><path d="M3 16.002v3.896C3 20.506 3.493 21 4.1 21H8a1 1 0 0 0 0-2.003H5v-2.995a1 1 0 1 0-2 0z"/></g></svg>' }, props));
};
exports.default = VidFullScreenOnIcon;