'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _index = require('../es5/index');

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var VidBackwardIcon = function VidBackwardIcon(props) {
  return _react2.default.createElement(_index2.default, _extends({ dangerouslySetGlyph: '<svg width="24" height="24" viewBox="0 0 24 24" focusable="false" role="presentation"><g fill="currentColor" fill-rule="evenodd"><path d="M12.249 4.174l-8.947 7.504c-.416.348-.4.904.033 1.242l8.88 6.914c.437.341.785.138.785-.446V4.601c0-.388-.147-.601-.366-.601-.113 0-.245.056-.385.174z"/><path d="M20.249 4.174l-8.947 7.504c-.416.348-.4.904.033 1.242l8.88 6.914c.437.341.785.138.785-.446V4.601c0-.388-.147-.601-.366-.601-.113 0-.245.056-.385.174z"/></g></svg>' }, props));
};
exports.default = VidBackwardIcon;