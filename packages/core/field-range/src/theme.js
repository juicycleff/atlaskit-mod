// @flow
import { colors } from '@atlaskit/theme';

export const thumb = {
  default: {
    background: colors.N0,
    border: colors.N800,
  },
  focus: {
    background: colors.N0,
    border: colors.B100,
  },
};

export const track = {
  default: {
    lower: colors.B400,
    upper: colors.N30A,
  },
  disabled: {
    lower: colors.N50,
    upper: colors.N30A,
  },
  hover: {
    lower: colors.B300,
    upper: colors.N40A,
  },
};
