# @atlaskit/website

## 2.1.2
- [patch] Clean Changelogs - remove duplicates and empty entries [e7756cd](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/e7756cd)
- [none] Updated dependencies [e7756cd](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/e7756cd)
  - @atlaskit/media-card@29.1.2
  - @atlaskit/media-filmstrip@9.0.3
  - @atlaskit/media-test-helpers@14.0.3
  - @atlaskit/media-core@19.1.3
  - @atlaskit/css-reset@2.0.6
  - @atlaskit/tooltip@10.2.1
  - @atlaskit/inline-dialog@7.1.2
  - @atlaskit/modal-dialog@5.2.2
  - @atlaskit/single-select@5.1.2
  - @atlaskit/field-text@6.0.4
  - @atlaskit/button@8.1.2
  - @atlaskit/page@7.1.1
  - @atlaskit/theme@4.0.4
  - @atlaskit/tag@5.0.4
  - @atlaskit/tag-group@5.1.1
  - @atlaskit/lozenge@5.0.4
  - @atlaskit/code@5.0.4
  - @atlaskit/spinner@7.0.2
  - @atlaskit/logo@8.1.2
  - @atlaskit/flag@8.1.1
  - @atlaskit/dynamic-table@9.1.2
  - @atlaskit/navigation@32.1.1
  - @atlaskit/layer-manager@4.2.1
  - @atlaskit/icon@12.1.2

## 2.1.1
- [patch] Update changelogs to remove duplicate [cc58e17](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/cc58e17)
- [none] Updated dependencies [cc58e17](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/cc58e17)
  - @atlaskit/quick-search@2.1.1
  - @atlaskit/media-card@29.1.1
  - @atlaskit/media-filmstrip@9.0.2
  - @atlaskit/media-test-helpers@14.0.2
  - @atlaskit/media-core@19.1.2
  - @atlaskit/css-reset@2.0.5
  - @atlaskit/theme@4.0.3
  - @atlaskit/layer-manager@4.1.1
  - @atlaskit/tag@5.0.3
  - @atlaskit/spinner@7.0.1
  - @atlaskit/single-select@5.1.1
  - @atlaskit/modal-dialog@5.1.1
  - @atlaskit/lozenge@5.0.3
  - @atlaskit/inline-dialog@7.1.1
  - @atlaskit/icon@12.1.1
  - @atlaskit/logo@8.1.1
  - @atlaskit/dynamic-table@9.1.1
  - @atlaskit/code@5.0.3
  - @atlaskit/button@8.1.1
  - @atlaskit/badge@8.0.3

## 2.1.0
- [patch] Updated dependencies [9d20f54](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9d20f54)
  - @atlaskit/spinner@7.0.0
  - @atlaskit/media-card@29.1.0
  - @atlaskit/media-filmstrip@9.0.1
  - @atlaskit/single-select@5.1.0
  - @atlaskit/navigation@32.1.0
  - @atlaskit/quick-search@2.1.0
  - @atlaskit/page@7.1.0
  - @atlaskit/inline-dialog@7.1.0
  - @atlaskit/modal-dialog@5.1.0
  - @atlaskit/layer-manager@4.1.0
  - @atlaskit/tooltip@10.2.0
  - @atlaskit/tag-group@5.1.0
  - @atlaskit/tag@5.0.2
  - @atlaskit/icon@12.1.0
  - @atlaskit/logo@8.1.0
  - @atlaskit/media-core@19.1.1
  - @atlaskit/media-test-helpers@14.0.1
  - @atlaskit/css-reset@2.0.4
  - @atlaskit/theme@4.0.2
  - @atlaskit/lozenge@5.0.2
  - @atlaskit/field-text@6.0.2
  - @atlaskit/code@5.0.2
  - @atlaskit/badge@8.0.2
  - @atlaskit/flag@8.1.0
  - @atlaskit/dynamic-table@9.1.0
  - @atlaskit/button@8.1.0

## 2.0.0
- [major] makes styled-components a peer dependency and upgrades version range from 1.4.6 - 3 to ^3.2.6 [1e80619](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e80619)
- [patch] Updated dependencies [1e80619](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1e80619)
  - @atlaskit/quick-search@2.0.0
  - @atlaskit/navigation@32.0.0
  - @atlaskit/page@7.0.0
  - @atlaskit/media-card@29.0.0
  - @atlaskit/media-filmstrip@9.0.0
  - @atlaskit/media-test-helpers@14.0.0
  - @atlaskit/media-core@19.0.0
  - @atlaskit/tooltip@10.0.0
  - @atlaskit/layer-manager@4.0.0
  - @atlaskit/modal-dialog@5.0.0
  - @atlaskit/flag@8.0.0
  - @atlaskit/icon@12.0.0
  - @atlaskit/dynamic-table@9.0.0
  - @atlaskit/tag@5.0.0
  - @atlaskit/tag-group@5.0.0
  - @atlaskit/single-select@5.0.0
  - @atlaskit/inline-dialog@7.0.0
  - @atlaskit/logo@8.0.0
  - @atlaskit/field-text@6.0.0
  - @atlaskit/button@8.0.0
  - @atlaskit/theme@4.0.0
  - @atlaskit/lozenge@5.0.0
  - @atlaskit/code@5.0.0
  - @atlaskit/badge@8.0.0
  - @atlaskit/spinner@6.0.0
  - @atlaskit/css-reset@2.0.3

## 1.5.24
- [patch] Updated dependencies [1c87e5a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/1c87e5a)
  - @atlaskit/page@6.0.4
  - @atlaskit/media-card@28.0.6
  - @atlaskit/media-filmstrip@8.0.9
  - @atlaskit/navigation@31.0.5
  - @atlaskit/quick-search@1.7.2

## 1.5.23
- [patch] Fix fullscreen examples increasing height on each browser repaint [5118ca4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/5118ca4)

## 1.5.22
- [patch] Updated dependencies [481c086](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/481c086)
  - extract-react-types-loader@0.1.3

## 1.5.21
- [patch] Updated dependencies [bd26d3c](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/bd26d3c)
  - @atlaskit/media-core@18.1.1
  - @atlaskit/media-test-helpers@13.0.1
  - @atlaskit/media-card@28.0.1

## 1.5.20
- [patch] Updated dependencies [84f6f91](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/84f6f91)
  - @atlaskit/media-test-helpers@13.0.0
  - @atlaskit/media-core@18.1.0
  - @atlaskit/media-filmstrip@8.0.7
  - @atlaskit/media-card@28.0.0
- [patch] Updated dependencies [9041d71](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/9041d71)
  - @atlaskit/media-test-helpers@13.0.0
  - @atlaskit/media-core@18.1.0
  - @atlaskit/media-filmstrip@8.0.7
  - @atlaskit/media-card@28.0.0

## 1.5.19
- [patch] Updated dependencies [d662caa](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/d662caa)
  - @atlaskit/icon@11.3.0
  - @atlaskit/quick-search@1.4.2
  - @atlaskit/media-filmstrip@8.0.6
  - @atlaskit/media-card@27.1.4
  - @atlaskit/tooltip@9.2.1
  - @atlaskit/page@6.0.3
  - @atlaskit/dynamic-table@8.0.3
  - @atlaskit/tag@4.1.1
  - @atlaskit/tag-group@4.0.1
  - @atlaskit/single-select@4.0.3
  - @atlaskit/navigation@31.0.4
  - @atlaskit/modal-dialog@4.0.5
  - @atlaskit/layer-manager@3.0.4
  - @atlaskit/inline-dialog@6.0.2
  - @atlaskit/flag@7.0.3
  - @atlaskit/logo@7.0.1
  - @atlaskit/field-text@5.0.3
  - @atlaskit/media-test-helpers@12.0.4
  - @atlaskit/media-core@18.0.3
  - @atlaskit/button@7.2.5
  - @atlaskit/theme@3.2.2
  - @atlaskit/code@4.0.4
  - @atlaskit/badge@7.1.2
  - @atlaskit/spinner@5.0.2
  - @atlaskit/css-reset@2.0.2
  - @atlaskit/lozenge@4.0.1

## 1.5.16
- [patch] Fix react dev warnings that appear when running the website in dev mode [b7f2a1a](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/b7f2a1a)

## 1.5.15
- [patch] release @atlaskit/navigation-next [33492df](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/33492df)

## 1.5.14
- [patch] Remove quick-search component from navigation. See docs for how to upgrade. [5447ec2](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/5447ec2)

## 1.5.10
- [patch] Bump to React 16.3. [4251858](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/4251858)

## 1.5.0
- [minor] Updated website to use iframe to load examples. Example loader now in a separate react app. Webpack config refactored to compile separate example loader, chunking refactored to be more performant with the new website changes. Updated modal-dialog to use new component structure to optionally specify a Body wrapping component. [e1fdfd8](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/e1fdfd8)

## 1.4.0
- [minor] Update styled-components dependency to support versions 1.4.6 - 3 [ceccf30](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/ceccf30)

## 1.3.5
- [patch] Migrate Navigation from Ak repo to ak mk 2 repo, Fixed flow typing inconsistencies in ak mk 2 [bdeef5b](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/bdeef5b)

## 1.3.3
- [patch] Resolved low hanging flow errors in field-base field-text comment icon item and website, $ [007de27](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/007de27)

## 1.3.1
- [patch] package bump to resolve discrepencies with npm [be745da](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/be745da)

## 1.3.0
- [minor] Add React 16 support. [12ea6e4](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/12ea6e4)

## 1.2.11
- [patch] Include fetch polyfill in website [7bb41ac](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/7bb41ac)

## 1.2.10
- [patch] Bug fix and better error messages for changeset error [7f09b86](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/7f09b86)

## 1.2.8
- [patch] update flow dep, fix flow errors  [722ad83](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/722ad83)
- [patch] update flow dep, fix flow errors  [722ad83](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/722ad83)

## 1.1.5
- [patch] bump icon dependency [da14956](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/da14956)
- [patch] bump icon dependency [da14956](https://bitbucket.org/atlassian/atlaskit-mk-2/commits/da14956)

## 1.1.1
- [patch] Use correct dependencies  [7b178b1](7b178b1)
- [patch] Use correct dependencies  [7b178b1](7b178b1)
